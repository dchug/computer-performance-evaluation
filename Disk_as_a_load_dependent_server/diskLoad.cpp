#include<iostream>
#include<cstdlib>
#include<math.h>

using namespace std;
int totalcyl = 8057;
//Generates a random number between 0 to 1.
double urn(void)
{
	return double(rand())/double(RAND_MAX);
}
/*
This is the uniform randomd generator function.We create the array of size 200 and intilize it with random
numbers. 
*/
double rng(void)
{
	static int n=200, firstcall=1;
	static double table[200];
	double rnumber;
	int i, itable;
	if (firstcall)
	{
		for(i=0; i<n; i++) table[i]=urn( ); // Any desired distribution
		firstcall= 0;
	}
	itable= int(n* urn( )); // Uniform selection from table
	rnumber= table[itable];
	table[itable] = urn( ); // Any desired distribution
	//cout<<"\tinside rng : "<<rnumber<<endl;
	return rnumber;
}
//This method checks the queue for the shortest distance cylinder reqest
int ShortestDistance(int dq[],int n,int current)
{
	int i=0;int min = totalcyl,result =0;
	
	for(;i<n;i++)
	{
		if(abs((dq[i])-current)<min)
		{
			min = abs((dq[i])-current);
			result = i;
		}
	}
	return result;
}

//calculated the time rquired to access the cylinder using the formula
double getTime(int x)
{
	double r = .3868,t = 1.5455,c = .3197;
	int xc = 1686;
	
	if(x<1)
	{
		return 0.0;
	}
	
	if(x<=xc)
	{
		return (t+c*pow((x-1),r));
	}
	else 
	{
		return (((c*r*(x-xc))/(pow((xc-1),(1-r))))+(t)+(c*pow((xc-1),r)));
	}
}

//gets the max possible distance to travel from the current cylinder
int getMax(int current)
{
	if(abs(current-1)>abs(current-totalcyl))
	{
		return abs(current-1);
	}
	else
	{
		return abs(current-totalcyl);
	}
}

int main()
{
	int Q =1;
	
	while(Q<21)
	{
		int dq[Q];
		int index = 0,cylinder =0,i=0;
			
		for(i=0;i<Q;i++) 
		{
			dq[i] = (int)(totalcyl*rng());
			//cout<<"Value in the dq = "<<dq[i]<<endl;
		}
		
		
		int current=1300,x=0,max = 0;
		double seekTime = 0.0,t = 0.0;
		unsigned long long seekDistance=0;
		//1000000 , 6
		for(i=0;i<1000000;i++)
		{
			index = ShortestDistance(dq,Q,current);
			cylinder = dq[index];
			
			dq[index] = (int)(totalcyl*rng());
			x = abs(cylinder-current);
			
			//cout<<"   cylinder is "<<cylinder<< " and the difference is "<<x<<endl;
			max = getMax(current);
		//	t += getTime(max)*sqrt((double)(x/(double)max));
			
		//	t += sqrt((double)(x/(double)max));
			//printf("\n\tRelative seek time %lf\n",t);
			seekDistance += x;
			seekTime += getTime(x);
			current = cylinder;
		}
		//t = pow(t,(double)(1/3));
		//printf("Array size = %d Average Seek time = %lf\n",Q,(t/5.0));
		cout<<"Array size = "<<Q<<" average seek distance is = "<<(seekDistance/1000000)<<" and average seek Time is = "<<(seekTime/(double)1000000)<<endl;
		Q++;
		
	}
	
	return 0;
}
