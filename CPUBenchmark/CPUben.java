

import java.util.*;

public class CPUben
{
	static int[] A =new int[5000000];
	static double[][] a = new double[400][800];
	static int nInt = 5000000;
	static int nDoub = 400;
	
	private static double time()
	{
		return (System.nanoTime()/1000000000);
	}
	static void MatrixInverse(int n)
	{
		int i,j,k;
		double t;
		for(i=0;i<n;i++)
		{
			t=a[i][i];
			for(j=i;j<2*n;j++)
				a[i][j]=a[i][j]/t;
			for(j=0;j<n;j++)
			{
				if(i!=j)
				{
					t=a[j][i];
					for(k=0;k<2*n;k++)
						a[j][k]=a[j][k]-t*a[i][k];
				}
			}
		}
	}
	
	private static int DOUBLE()
	{
		int i,j,k,n = nDoub;
		double t;
		double time1=0.0,time2=0.0,total = 0.0;
		int countDouble =0;
		while(total<10.0)
		{
			for(i=0;i<n;i++)
			{
				for(j=0;j<n;j++)
				{
					if(i==j)
					{
						a[i][j] = 2.0001;
					}
					else
					{
						a[i][j] = 1.0001;
					}
				}
			}
         
			for(i=0;i<n;i++)
			{
				for(j=n;j<2*n;j++)
				{
					if(i==j-n)
						a[i][j]=1.0;
					else
						a[i][j]=0.0;
				}
			}
			time1 = time();
			MatrixInverse(n);
			time2 = time()-time1;
			total+=time2;
			countDouble++;
		}
		System.out.println("\tTotal time taken by the double operation is -> "+total);
		return countDouble;
	}
	
	private static void quickSort(int arr[], int left, int right)
	{
		int i = left, j = right;
		int tmp;
		int pivot = arr[(left + right) / 2];
 
 
		while (i <= j)
		{
			while (arr[i] < pivot)
                  i++;
            while (arr[j] > pivot)
                  j--;
            if (i <= j) {
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      };
 

		if (left < j)
            quickSort(arr, left, j);
		if (i < right)
            quickSort(arr, i, right);
	}
	
	private static int INTEGER()
	{
		double time1=0.0,time2=0.0,total = 0.0;
		int countInt = 0;
		while(total<10.0)
		{
			//int A[1000];
			int M = nInt,kmax = nInt,n=0;
			int k;
			for(k=0; k<kmax; k++) A[k]=n=n+1-n/M*M;
		
			//for(int k=0; k<kmax; k++) A[k]= nInt-k;	
	
			time1 = time();
			quickSort(A,0,nInt-1);
			time2 = time()-time1;
			total+=time2;
			countInt++;
			//printf("%lf",total);
		}
		System.out.println("\tTotal time taken by the Integer operation is ->"+total);
		return countInt;
	}
	
	public static void main(String args[])
	{
		double c1 =0,c2=0;
		double v1 = 0.0,v2 =0.0,v = 0.0;
		
		
			c1 = DOUBLE();
			c2 = INTEGER();
		
		
		v1 = 60.0*(c1/10.0);
		v2 = 60.0*(c2/10.0);
		
		System.out.println("\tTotal Number of Double operations-> "+c1+"\n\tTotoal Number of Integer operations-> "+c2);
		System.out.println("\tSpeed of Double operations-> "+v1+" operations/minute\n\tSpeed of Integer operations-> "+v2+" operations/minute");
		
		v = (2*v1*v2)/(v1+v2);
		System.out.println("\n\n\tAverage speed of both operations -> "+v+"operations/minute\n");
	
	}
}