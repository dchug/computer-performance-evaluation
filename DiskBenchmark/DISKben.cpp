#include<iostream>
#include<ctime>
#include<fstream>
#include<string.h>
#include<cstdlib>

using namespace std;


char filename[50] = "file.dat";

const int BytesPerRecord = 64;

struct client
{
	int account;
	char name[BytesPerRecord - 8];
	int balance;
};

client c;
int n = 0, fsize = 0;
//global variables which keeps the variable
double Vsew = 0.0, Vser = 0.0, Vrar = 0.0;
unsigned long long int checksum = 0;


double time()
{
	return ((double)clock() / CLOCKS_PER_SEC);
}
//Function to calculate the filesize
int fileSize(const char *add)
{
	ifstream mySource;
	mySource.open(add, ios_base::binary);
	mySource.seekg(0, ios_base::end);
	int size = (int)mySource.tellg();
	mySource.close();
	return size;
}

//method for seqential write. Called from the DSEQ().
double seqWrite()
{
	cout << "Sequential binary file diskmark.dat "<< endl;
	cout << "-------------------------------------------------------- " << endl;
	cout << "Time assigned for making file:      10 sec" << endl;

	double total = 0.0, t1 = 0.0;
	int i = 0;
	ofstream OS;
	char name[] = "Account number";

	t1 = time();
	OS.open(filename, ios::out | ios::binary);
	total += (time() - t1);

	while (total<10.0)
	{
		t1 = time();
		c.account = 3434343;
		while(i<55)
		{
			c.name[i] = 'a';
			i++;
		}
		c.name[56] = 0;
		c.balance = 4344;
		OS.write(reinterpret_cast<const char*>(&c), sizeof(c));
		total += (time() - t1);
		checksum += 4344;
		n++;
	}
	        
	cout << "Time used for making file:          " << total << " sec" << endl;

	t1 = time();
	OS.close();
	total += (time() - t1);

	fsize = fileSize(filename);
	fsize = fsize / (1024 * 1024);

	Vsew = (double)fsize / total;
	        
	cout << "Total Time:                         " << total << " sec" << endl;
	cout << "Number of records created:          " << n << endl;
	cout << "check sum is:                       "<< checksum <<endl;
	cout << "File size :                         " << fsize << " MB" << endl;
	cout << "Effective sequential write speed :  " << Vsew << " MB/sec" << endl;
	
	return total;
}



double DSEQ()
{
	double total = 0.0, t1 = 0.0;
	int recordCount = 0;
	int account = 0, balance = 0 ;
	unsigned long long int checkBalance = 0;


	seqWrite();
	//cout << "\n";
	        
	cout << "\nSequential read for the binary file diskmark.dat "<< endl;
	cout << "-------------------------------------------------------- " << endl;
	ifstream in;
	total = 0.0;
	int i = 0;
	char name[56];
	
	t1 = time();
	in.open(filename, ios::out | ios::binary);
	total += (time() - t1);
	
	//main loop for sequential read
	while (!in.eof())
	{
		t1 = time();

		in.read(reinterpret_cast<char*>(&c), sizeof(c));
		account = c.account;
		balance = c.balance;
		while (i<56)
		{
			name[i] = c.name[i];
			i++;
		}
		total += (time()- t1);

		checkBalance += balance;
		
		recordCount++;
	}
	//cout << "\nbalance is" << balance<<endl;
	t1 = time();
	in.close();
	total += (time() - t1);

	Vser = (double)fsize / total;
	        
	cout << "Time used to reading the file:      " << total << " sec" << endl;
	cout << "Number of records read/processed:   " << recordCount << endl;
	cout << "check sum is:                       " << checkBalance << endl;
	cout << "File size [megabytes processed]:    " << fsize << " MB" << endl;
	cout << "Effective sequential read speed:    " << Vser << " MB/sec" << endl;
	
	//remove(filename);
	return total;
}
//Random read method
double DRAN()
{
	double total = 0.0, t1 = 0.0;
	int account = 0, balance = 0, i = 0; char name[56];
	unsigned long long int checkBalance = 0;

	//seqWrite();
	cout << "\nRandom read from the binary file diskmark.dat "<< endl;
	cout << "-------------------------------------------------------- " << endl;
	cout << "Time assigned for reading file:     10 sec" << endl;
	ifstream in;
	in.open(filename, ios::in | ios::out | ios::binary);
	int N = 0;
	//main loop for reandom read
	while (total<10.0)
	{
		int record = rand()%n;
		in.seekg(record*sizeof(c));

		t1 = time();
		in.read(reinterpret_cast<char*>(&c), sizeof(c));
		account = c.account;
		balance = c.balance;
		while (i<56)
		{
			name[i] = c.name[i];
			i++;
		}
		total += (time() - t1);
		//cout<<"balance"<<balance;
		checkBalance += balance;
		N++;
	}

	t1 = time();
	in.close();
	total += (time()-t1);
	
	double recordSize = (double)fsize/n;
	Vrar = (recordSize*N)/total;
	        
	cout << "Total Time:                         " << total << endl;
	cout << "Number of records read/processed:   "<<N<< endl;
	cout << "check sum is:                       " << checkBalance << endl;
	cout << "Random access per second:           " <<N/total << endl;
	cout << "Megabytes processed:                " << recordSize*N<< " MB" << endl;
	cout << "Effective Random read speed:        " << Vrar << " MB/sec" << endl;
	cout << "-------------------------------------------------------- " << endl;

	return total;
}

int main()
{
	double c1 = 0, c2 = 0;
	double v = 0.0;

	c1 = DSEQ();
	c2 = DRAN();

	v = 3 / ((1/Vsew)+(1/Vser)+(1/Vrar));
	//cout << "---------------------------------------------"<<endl;
	     //"Time assigned for making file:      "
	cout <<"DiskMark Benchmark:                 "<<v<<" MB/sec\t";
	

	return 0;
}
