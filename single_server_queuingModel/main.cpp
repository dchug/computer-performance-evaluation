//
//  main.cpp
//  GG1
//
//  Created by Ketan Kapre on 11/13/16.
//  Copyright © 2016 Ketan Kapre. All rights reserved.
//

#include <iostream>
#include <math.h>
#include <iomanip>

using namespace std;

double urn(void)
{
    return double(rand())/double(RAND_MAX);
}
double FINV(double u, double A,double S)
{
    return (sqrt(S*S+2.*A*u));
}

double rng(int Id, double A, double S)
{
    double u = urn();
    double rn =0.0;
    switch(Id)
    {
            //Normal distribution
        case 1:
            for(int i=2;i<12;i++)
            {
                u = u+urn();
            }
            rn = A+S*(u-6.0);
            break;
            //Uniform distribution
        case 2:
            rn = A+u*(S-A);
            break;
            //Exponential distribution
        case 3:
            rn = -A * log(u);
            break;
            //Arbitrary distribution
        case 4:
            rn = FINV(u,A,S);
            break;
            //constant value
        case 5:
            rn = A;
            break;
        default:
            cout<<"You entered an invalid id";
            break;
    }
    return rn;
}

int INC(int K,int Qsize)
{
    K = K+1-(K/Qsize*Qsize);
    return K;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    
   
    double JPrint =10000,JMax =10000,ida = 2,aa=6.0,sa=10.0,ids=2,as=0.0,ss=4.0; //to be read from the user.
    
    int Qsize = 500,in = 0,out =0,JOBA = 0, JOBS =0,NJOB =0;
    
    double Tbusy= 0.0, Jobsec =0.0,SDJobs =0.0,TNA =0.0, TND =0.0, ATS =0.0, SDTS =0.0,ATA=0.0,SDTA =0.0,ART = 0.0,SDRT =0.0,DELTAT = 0.0,Time =0.0,ta =0.0,ts =0.0;
    double QA[Qsize],QS[Qsize];
    
    cout<<"JOBS"<<setw(12)<<"Time"<<setw(12)<<"ATA"<<setw(12)<<"SDTA"<<setw(12)<<"ATS"<<setw(12)<<"SDTS"<<setw(12)<<"ART"<<setw(12)<<"SDRT"<<setw(12)<<"AJOB"<<setw(12)<<"SDJOB"<<setw(12)<<"U"<<setw(12)<<"Y"<<setw(12)<<endl;
    
    while(JMax<200001)
   {
       
        TNA:
            if(TND<TNA)
                goto TND;
        
            NJOB = JOBA - JOBS;
            
            if(NJOB<Qsize)
                goto rng30;
            cout<<" queue overflow at time = "<<TNA<<" JOBA = "<<JOBA<<"JOBS ="<<JOBS<<endl;
    
        rng30:
            do{
                ta = rng(ida,aa,sa);
            }while(ta<=0.0);
    
            //ts=double ts = 0.0;
            do{
                ts = rng(ids,as,ss);
            }while(ts<=0.0);
    
            JOBA = JOBA +1;
            ATA = ATA+ta;
            SDTA = SDTA+pow(ta,2);
            
            if(NJOB>0)
                goto update;
            TND = TNA+ts;
            goto queue;
            
        update: DELTAT = TNA - Time;
            Tbusy = Tbusy+DELTAT;
            Jobsec = Jobsec+DELTAT*NJOB;
            SDJobs = SDJobs+(DELTAT*NJOB)*NJOB;
            
        queue: Time = TNA;
            TNA = Time+ta;
            QA[in] = Time;
            QS[in] = ts;
            in = INC(in,Qsize);
        goto TNA;
        
    TND: NJOB = JOBA-JOBS;
        DELTAT = TND - Time;
        Tbusy = Tbusy+DELTAT;
        Jobsec = Jobsec+DELTAT*NJOB;
        SDJobs = SDJobs+(DELTAT*NJOB)*NJOB;
        Time = TND;
        JOBS = JOBS+1;
        ATS = ATS+QS[out];
        SDTS = SDTS+pow(QS[out],2);
        ART = ART+Time-QA[out];
        SDRT = SDRT+pow((Time-QA[out]),2);
        out = INC(out,Qsize);
        //cout<<"Running";
        
        if(JOBS==JOBA)
            goto define;
        TND = Time + QS[out];
        goto compute;
    define: TND = TNA;
    compute: if(JOBS!=(JOBS/(JPrint*JPrint)) && JOBS!=JMax)
                goto TNA;
            //cout<<"\nATS is  "<<ATS<<"SDTS is "<<SDTS<<endl;
       
            double U = Tbusy/Time;
            double AJob = Jobsec/Time;
            double XSDJob = sqrt(SDJobs/(Time-pow(AJob,2)));
            double XATS = ATS/JOBS;
            double XSDTS = sqrt(SDTS/JOBS-pow(XATS,2));
            double XATA = ATA/JOBA;
            double XSDTA = sqrt(SDTA/JOBA-pow(XATA,2));
            double XART = ART/JOBS;
            double XSDRT = sqrt(SDRT/JOBS-pow(XART,2));
            double Y = JOBS/Time;
//            cout<<JOBS<<"\t"<<Time<<"\t"<<XATA<<"\t"<<XSDTA<<"\t"<<XATS<<"\t"<<XSDTS<<"\t"<<XART<<"\t"
//            <<XSDRT<<"\t"<<AJob<<"\t"<<XSDJob<<"\t"<<U<<"\t"<<Y;
            cout<<"\n"<<JOBS<<setw(12)<<Time<<setw(12)<<XATA<<setw(12)<<XSDTA<<setw(12)<<XATS<<setw(12)<<XSDTS<<setw(12)<<XART<<setw(12)<<XSDRT<<setw(12)<<AJob<<setw(12)<<XSDJob<<setw(12)<<U<<setw(12)<<Y<<endl;
            if(JOBS<JMax)
                goto TNA;
       JMax   += 10000;
                
    }

    std::cout << "\nHello, World!\n";
    return 0;
}




















